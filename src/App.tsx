import * as React from 'react';
import './App.css';

const logo = require('./logo.svg');

export interface Props {
}

export interface State {
  income: string;
}

class App extends React.Component<Props, State> {

  constructor() {
    super();
    this.state = {
      income: ''
    };
    // メソッド内でthisを見るためのお約束
    this.onChangeIncome = this.onChangeIncome.bind(this);
    this.moneyYouWant = this.moneyYouWant.bind(this);
  }

  onChangeIncome(e: React.FormEvent<HTMLInputElement>) {
    this.setState({income: e.currentTarget.value});
  }

  moneyYouWant(money: number): string {
    if (money === NaN || money <= 0) {
      return '';
    }
    return `${money * 10000000}兆円欲しい`;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          <label>現在の年収 <input type="text" value={this.state.income} onChange={this.onChangeIncome} />万円</label>
        </p>
        <p className="Money">{this.moneyYouWant(Number(this.state.income))}</p>
      </div>
    );
  }
}

export default App;
